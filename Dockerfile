# specify the node base image with your desired version node:<version>
FROM node:10

# replace this with your application's default port
EXPOSE 8000

RUN mkdir /usr/src/app && \
    git clone https://bdausses@bitbucket.org/bdausses/tf_demo_app.git /usr/src/app && \
    cd /usr/src/app && \
    npm install

# Need ADD command here to add the correct cloud_logo.svg
ADD public/images/cloud_logo.svg /usr/src/app/public/images/cloud_logo.svg

WORKDIR /usr/src/app
ENTRYPOINT ["/usr/local/bin/node", "index.js"]
