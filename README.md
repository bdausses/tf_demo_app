# Terraform Sample Node App

This simple [Express](https://expressjs.com/) app is an example of how to run a Node.js application in Docker, provisioned by Terraform.

### Prerequisites

To run this application, you'll need to:

* [Install Docker](https://www.docker.com/community-edition) (if you're on Mac or Windows)
* Clone this repository:

      $ git clone <REPO>
      $ cd tf_demo_app


### Running the Package with Docker

TODO
